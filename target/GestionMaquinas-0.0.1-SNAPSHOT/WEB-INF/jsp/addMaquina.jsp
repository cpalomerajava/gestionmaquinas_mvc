<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Crear máquina</title>

<!-- Bootstrap -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">
<style>
body {
	margin-top: 30px;
}
select{
	height: 25px;
}

#button{
    margin-left: 77px;
    margin-top: 34px;
    width: 87px;
    height: 41px;
}

#marco{
	margin-top: -30px;
}

#intro{
    margin-left: 40%;
}
#title{
	padding-bottom: 20px;
    font-size: x-large;
    text-decoration: underline;
}

</style>
</head>
<body>
	<div id="marco" class="jumbotron">
		<div id="intro" class="container">
	
			<div id="title">${headerMessage}</div>	
			
		        <form:form method="POST" action="addmaquina" modelAttribute="maquina">
		        
		        	<form:hidden path="id" /> 
		             <table>
		                <tr>
		                    <td><form:label path="nombre">Nombre</form:label></td>
		                    <td><form:input path="nombre"/></td>
		                </tr>
		                <tr>
		                    <td><form:label path="ip">Dirección IP</form:label></td>
		                    <td><form:input path="ip"/></td>
		                </tr>
		                <tr>
		                    <td><form:label path="categoria">Categoria</form:label></td>
		                    <td><form:input path="categoria"/></td>
		                </tr>
		                <tr>
		                    <td><input id="button" class="btn btn-primary btn-lg" type="submit" value="Guardar"/></td>
		                </tr>
		            </table>
		        </form:form>
		</div>
	</div>
	

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>