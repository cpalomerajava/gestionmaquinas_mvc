package es.cic.gestionmaquinas.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "responsable")
public class Responsable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String userName;
    private String nombre;


    public Long getId()
    {

	return id;
    }


    public void setId(Long id)
    {

	this.id = id;
    }


    public String getUserName()
    {

	return userName;
    }


    public void setUserName(String userName)
    {

	this.userName = userName;
    }


    public String getNombre()
    {

	return nombre;
    }


    public void setNombre(String nombre)
    {

	this.nombre = nombre;
    }


    @Override
    public String toString()
    {

	return "Responsable [id=" + id + ", userName=" + userName + ", nombre=" + nombre + "]";
    }

}
