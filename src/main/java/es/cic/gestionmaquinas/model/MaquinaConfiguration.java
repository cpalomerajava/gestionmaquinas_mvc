package es.cic.gestionmaquinas.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "maquinaConfiguration")
public class MaquinaConfiguration
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "makina_configuration_id")
    private long id;
    private String maquina;
    private String ultimoScriptBBDD;
    private boolean cambioWars;
    private boolean cambioEntorno;
    private boolean updateBbdd;
    private String fecha;
    private String responsable;
    private String versionWar;
    private boolean active;


    public MaquinaConfiguration(long id, String maquina, String ultimoScriptBBDD, boolean cambioWars, boolean cambioEntorno, boolean updateBbdd, String fecha, String responsable, String versionWar,
	    boolean active)
    {

	super();
	this.id = id;
	this.maquina = maquina;
	this.ultimoScriptBBDD = ultimoScriptBBDD;
	this.cambioWars = cambioWars;
	this.cambioEntorno = cambioEntorno;
	this.updateBbdd = updateBbdd;
	this.fecha = fecha;
	this.responsable = responsable;
	this.versionWar = versionWar;
	this.active = active;
    }


    public MaquinaConfiguration()
    {

	super();
    }


    public long getId()
    {

	return id;
    }


    public void setId(long id)
    {

	this.id = id;
    }


    public String getMaquina()
    {

	return maquina;
    }


    public void setMaquina(String maquina)
    {

	this.maquina = maquina;
    }


    public String getUltimoScriptBBDD()
    {

	return ultimoScriptBBDD;
    }


    public void setUltimoScriptBBDD(String ultimoScriptBBDD)
    {

	this.ultimoScriptBBDD = ultimoScriptBBDD;
    }


    public boolean isCambioWars()
    {

	return cambioWars;
    }


    public void setCambioWars(boolean cambioWars)
    {

	this.cambioWars = cambioWars;
    }


    public boolean isCambioEntorno()
    {

	return cambioEntorno;
    }


    public void setCambioEntorno(boolean cambioEntorno)
    {

	this.cambioEntorno = cambioEntorno;
    }


    public boolean isUpdateBbdd()
    {

	return updateBbdd;
    }


    public void setUpdateBbdd(boolean updateBbdd)
    {

	this.updateBbdd = updateBbdd;
    }


    public String getFecha()
    {

	return fecha;
    }


    public void setFecha(String fecha)
    {

	this.fecha = fecha;
    }


    public String getResponsable()
    {

	return responsable;
    }


    public void setResponsable(String responsable)
    {

	this.responsable = responsable;
    }


    public String getVersionWar()
    {

	return versionWar;
    }


    public void setVersionWar(String versionWar)
    {

	this.versionWar = versionWar;
    }


    public boolean isActive()
    {

	return active;
    }


    public void setActive(boolean active)
    {

	this.active = active;
    }


    @Override
    public String toString()
    {

	return "MaquinaConfiguration [id=" + id + ", maquina=" + maquina + ", ultimoScriptBBDD=" + ultimoScriptBBDD + ", cambioWars=" + cambioWars + ", cambioEntorno=" + cambioEntorno + ", updateBbdd=" + updateBbdd + ", fecha=" + fecha + ", responsable=" + responsable + ", versionWar=" + versionWar + ", active=" + active + "]";
    }

}
