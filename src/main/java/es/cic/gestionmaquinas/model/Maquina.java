package es.cic.gestionmaquinas.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "maquinas")
public class Maquina
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String ip;
    private String categoria;


    public Maquina(Long id, String nombre, String ip, String categoria)
    {

	super();
	this.id = id;
	this.nombre = nombre;
	this.ip = ip;
	this.categoria = categoria;
    }


    public Maquina()
    {

	super();
    }


    public Long getId()
    {

	return id;
    }


    public void setId(Long id)
    {

	this.id = id;
    }


    public String getNombre()
    {

	return nombre;
    }


    public void setNombre(String nombre)
    {

	this.nombre = nombre;
    }


    public String getIp()
    {

	return ip;
    }


    public void setIp(String ip)
    {

	this.ip = ip;
    }


    public String getCategoria()
    {

	return categoria;
    }


    public void setCategoria(String categoria)
    {

	this.categoria = categoria;
    }


    @Override
    public String toString()
    {

	return "Maquina [id=" + id + ", nombre=" + nombre + ", ip=" + ip + ", categoria=" + categoria + "]";
    }

}
