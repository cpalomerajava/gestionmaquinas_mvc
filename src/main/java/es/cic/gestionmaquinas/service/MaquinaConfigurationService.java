package es.cic.gestionmaquinas.service;


import java.util.List;
import es.cic.gestionmaquinas.model.MaquinaConfiguration;


public interface MaquinaConfigurationService
{

    public List<MaquinaConfiguration> getAllMaquinaConfiguration();


    public List<MaquinaConfiguration> getActiveMaquinaConfiguration();


    public MaquinaConfiguration getMaquinaConfigurationById(Long id);


    public boolean saveMaquinaConfiguration(MaquinaConfiguration maquinaConfiguration);


    public boolean deleteMaquinaConfigurationById(Long id);


    List<MaquinaConfiguration> findHistoricOfMakinaActive(String maquina);


    public List<MaquinaConfiguration> getActiveMaquinaConfigurationByNombre(String maquina);


    public List<MaquinaConfiguration> getAllMaquinaConfigurationByName(String maquina);

}