package es.cic.gestionmaquinas.service;


import java.util.List;
import es.cic.gestionmaquinas.model.Responsable;


public interface ResponsableService
{

    public List<Responsable> getAllResponsable();


    public Responsable getResponsableById(Long id);


    public boolean saveResponsable(Responsable responsable);


    public boolean deleteResponsableById(Long id);


    public List<Responsable> getResponsablebyUserNamePartial(String userName);


    public List<String> getResponsableUsername();

}