package es.cic.gestionmaquinas.service;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import es.cic.gestionmaquinas.model.Responsable;
import es.cic.gestionmaquinas.repository.ResponsableRepository;


@Service
@Transactional
public class ResponsableServiceImpl implements ResponsableService
{

    @Autowired
    private ResponsableRepository repository;


    @Override
    public List<Responsable> getAllResponsable()
    {

	List<Responsable> list = new ArrayList<Responsable>();
	repository.findAll().forEach(e -> list.add(e));
	return list;
    }


    @Override
    public Responsable getResponsableById(Long id)
    {

	Responsable responsable = repository.findOne(id);
	return responsable;
    }


    @Override
    public boolean saveResponsable(Responsable responsable)
    {

	try
	{
	    repository.save(responsable);
	    return true;
	}
	catch (Exception ex)
	{
	    return false;
	}
    }


    @Override
    public boolean deleteResponsableById(Long id)
    {

	try
	{
	    repository.delete(id);
	    return true;
	}
	catch (Exception ex)
	{
	    return false;
	}

    }


    @Override
    public List<Responsable> getResponsablebyUserNamePartial(String userName)
    {

	return repository.getResponsablebyUserNamePartial(userName);
    }


    @Override
    public List<String> getResponsableUsername()
    {

	return repository.getResponsableUsername();
    }

}
