package es.cic.gestionmaquinas.service;


import java.util.List;
import es.cic.gestionmaquinas.model.Maquina;


public interface MaquinaService
{

    public List<Maquina> getAllMaquinas();


    public Maquina getMaquinaById(Long id);


    public boolean saveMaquina(Maquina maquina);


    public boolean deleteMaquinaById(Long id);


    public List<String> getListaMaquinasFormateada();


    public List<Maquina> getMaquinabyNamePartial(String maquina);

}