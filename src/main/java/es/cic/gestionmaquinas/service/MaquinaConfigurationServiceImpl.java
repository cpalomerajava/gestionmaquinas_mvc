package es.cic.gestionmaquinas.service;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import es.cic.gestionmaquinas.model.MaquinaConfiguration;
import es.cic.gestionmaquinas.repository.MaquinaConfigurationRepository;


@Service
@Transactional
public class MaquinaConfigurationServiceImpl implements MaquinaConfigurationService
{

    @Autowired
    private MaquinaConfigurationRepository repository;


    @Override
    public List<MaquinaConfiguration> getAllMaquinaConfiguration()
    {

	List<MaquinaConfiguration> list = new ArrayList<MaquinaConfiguration>();
	repository.findAll().forEach(e -> list.add(e));
	return list;
    }


    @Override
    public List<MaquinaConfiguration> getActiveMaquinaConfiguration()
    {

	List<MaquinaConfiguration> list = repository.findAllMakinaActive();
	// repository.findAll().forEach(e -> list.add(e));
	// list.stream().filter(maquinaConfiguration ->
	// !maquinaConfiguration.isActive()).forEach(e -> list.remove(e));
	return list;
    }


    @Override
    public MaquinaConfiguration getMaquinaConfigurationById(Long id)
    {

	MaquinaConfiguration maquinaConfiguration = repository.findOne(id);
	return maquinaConfiguration;
    }


    @Override
    public boolean saveMaquinaConfiguration(MaquinaConfiguration maquinaConfiguration)
    {

	try
	{
	    // desactivo el anterior
	    MaquinaConfiguration config = repository.findMakinaActive(maquinaConfiguration.getMaquina());
	    if(config != null)
	    {
		config.setActive(false);
		repository.save(config);
	    }
	    //
	    maquinaConfiguration.setFecha(getTodayDate());
	    maquinaConfiguration.setActive(true);
	    repository.save(maquinaConfiguration);
	    return true;
	}
	catch (Exception ex)
	{
	    System.out.println(ex);
	    return false;
	}
    }


    @Override
    public boolean deleteMaquinaConfigurationById(Long id)
    {

	try
	{
	    repository.delete(id);
	    return true;
	}
	catch (Exception ex)
	{
	    return false;
	}

    }


    @Override
    public List<MaquinaConfiguration> findHistoricOfMakinaActive(String maquina)
    {

	try
	{

	    List<MaquinaConfiguration> lista = repository.findHistoricOfMakinaActive(maquina);
	    return lista;
	}
	catch (Exception e)
	{
	    System.out.println(e);
	    return null;
	}
    }


    private String getTodayDate()
    {

	Date today = new Date();
	// formatting date in Java using SimpleDateFormat
	SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	String date = DATE_FORMAT.format(today);

	return date;
    }


    @Override
    public List<MaquinaConfiguration> getActiveMaquinaConfigurationByNombre(String maquina)
    {

	return repository.getActiveMaquinaConfigurationByNombre(maquina);
    }


    @Override
    public List<MaquinaConfiguration> getAllMaquinaConfigurationByName(String maquina)
    {

	return repository.getAllMaquinaConfigurationByName(maquina);
    }

}
