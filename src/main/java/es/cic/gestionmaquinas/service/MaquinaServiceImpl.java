package es.cic.gestionmaquinas.service;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import es.cic.gestionmaquinas.model.Maquina;
import es.cic.gestionmaquinas.repository.MaquinaRepository;


@Service
@Transactional
public class MaquinaServiceImpl implements MaquinaService
{

    @Autowired
    private MaquinaRepository repository;


    @Override
    public List<Maquina> getAllMaquinas()
    {

	List<Maquina> list = new ArrayList<Maquina>();
	repository.findAll().forEach(e -> list.add(e));
	return list;
    }


    @Override
    public Maquina getMaquinaById(Long id)
    {

	Maquina maquina = repository.findOne(id);
	return maquina;
    }


    @Override
    public boolean saveMaquina(Maquina maquina)
    {

	try
	{
	    repository.save(maquina);
	    return true;
	}
	catch (Exception ex)
	{
	    return false;
	}
    }


    @Override
    public boolean deleteMaquinaById(Long id)
    {

	try
	{
	    repository.delete(id);
	    return true;
	}
	catch (Exception ex)
	{
	    return false;
	}

    }


    @Override
    public List<String> getListaMaquinasFormateada()
    {

	List<String> StringList = new ArrayList<String>();
	List<Maquina> listaMaquinas = getAllMaquinas();
	for (Maquina maquina : listaMaquinas)
	{
	    StringList.add(maquina.getNombre() + " (" + maquina.getIp() + " )");
	}

	return StringList;
    }


    @Override
    public List<Maquina> getMaquinabyNamePartial(String maquina)
    {

	return repository.getMaquinabyNamePartial(maquina);
    }

}
