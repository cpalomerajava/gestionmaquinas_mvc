package es.cic.gestionmaquinas.controller;


import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import es.cic.gestionmaquinas.model.Maquina;
import es.cic.gestionmaquinas.model.Responsable;
import es.cic.gestionmaquinas.service.MaquinaService;
import es.cic.gestionmaquinas.service.ResponsableService;


@Controller
public class MaquinaController
{

    private static Logger log = Logger.getLogger(MaquinaController.class);

    @Autowired
    private MaquinaService maquinaService;

    @Autowired
    private ResponsableService responsableService;


    /**
     * devolver lista maquinas
     * 
     * @return
     */
    @RequestMapping(value = "/listallMaquinas", method = RequestMethod.GET)
    public ModelAndView displayAllMaquinasList()
    {

	log.info("Mostrar todas las maquinas");
	ModelAndView mv = new ModelAndView();
	List<Maquina> maquinaList = maquinaService.getAllMaquinas();
	List<Responsable> responsableList = responsableService.getAllResponsable();
	mv.addObject("maquinaList", maquinaList);
	mv.addObject("responsableList", responsableList);
	mv.setViewName("allMaquinas");
	return mv;
    }


    // editar Maquina
    @RequestMapping(value = "/editmaquina/{id}", method = RequestMethod.GET)
    public ModelAndView displayEditMaquinaForm(@PathVariable Long id)
    {

	ModelAndView mv = new ModelAndView("/editMaquina");
	Maquina maquina = maquinaService.getMaquinaById(id);
	mv.addObject("headerMessage", "Editar m�quina");
	mv.addObject("maquina", maquina);
	return mv;
    }


    @RequestMapping(value = "/editmaquina/{id}", method = RequestMethod.POST)
    public ModelAndView saveEditedMaquina(@ModelAttribute Maquina maquina, BindingResult result)
    {

	ModelAndView mv = new ModelAndView("redirect:/listallMaquinas");

	if(result.hasErrors())
	{
	    System.out.println(result.toString());
	    return new ModelAndView("error");
	}
	boolean isSaved = maquinaService.saveMaquina(maquina);
	if(!isSaved)
	{

	    return new ModelAndView("error");
	}

	return mv;
    }


    @RequestMapping(value = "/deletemaquina/{id}", method = RequestMethod.GET)
    public ModelAndView deleteMaquinaById(@PathVariable Long id)
    {

	boolean isDeleted = maquinaService.deleteMaquinaById(id);
	System.out.println("maquina deletion respone: " + isDeleted);
	ModelAndView mv = new ModelAndView("redirect:/listallMaquinas");
	return mv;

    }

    // Fin editar maquina


    // a�adir maquina

    @RequestMapping(value = "/addmaquina", method = RequestMethod.GET)
    public ModelAndView displayNewMaquinaForm()
    {

	ModelAndView mv = new ModelAndView("/addMaquina");
	mv.addObject("headerMessage", "Nueva M�quina");
	mv.addObject("maquina", new Maquina());
	return mv;
    }


    @RequestMapping(value = "/addmaquina", method = RequestMethod.POST)
    public ModelAndView saveNewMaquina(@ModelAttribute Maquina maquina, BindingResult result)
    {

	ModelAndView mv = new ModelAndView("redirect:/listallMaquinas");

	if(result.hasErrors())
	{
	    return new ModelAndView("error");
	}
	boolean isAdded = maquinaService.saveMaquina(maquina);
	if(isAdded)
	{
	    mv.addObject("message", "Nueva m�quina a�adida exitosamente");
	}
	else
	{
	    return new ModelAndView("error");
	}

	return mv;
    }

    // fin a�adir maquina


    // buscador maquina

    @RequestMapping(value = "/findmaquina", method = RequestMethod.GET)
    public ModelAndView searchMaquina(@RequestParam(name = "nombre", required = false) String nombre, Model model)
    {

	ModelAndView mv = new ModelAndView("allMaquinas");
	List<Maquina> result = null;
	log.info("Buscar M�quina con el nombre: " + nombre);
	if(nombre.isEmpty())
	    result = maquinaService.getAllMaquinas();
	else if(!nombre.isEmpty())
	    result = maquinaService.getMaquinabyNamePartial(nombre);

	if(result.size() > 0)
	{
	    model.addAttribute("maquinaList", result);
	    log.info("Encontrado  m�quinas con el nombre: " + nombre);
	}
	else
	{
	    model.addAttribute("error", "No se han encontrado resultados");
	    log.info("No encontrado m�quinas con el nombre: " + nombre);
	}
	List<Responsable> responsableList = responsableService.getAllResponsable();
	mv.addObject("responsableList", responsableList);

	return mv;
    }

    // fin buscador maquinas

    // Get All Users
    // @RequestMapping(value = "/allUsers", method = RequestMethod.POST)
    // public ModelAndView displayAllUser()
    // {
    //
    // System.out.println("User Page Requested : All Users");
    // ModelAndView mv = new ModelAndView();
    // List<User> userList = userService.getAllUsers();
    // mv.addObject("userList", userList);
    // mv.setViewName("allUsers");
    // return mv;
    // }
    //
    //
    // @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    // public ModelAndView displayNewUserForm()
    // {
    //
    // ModelAndView mv = new ModelAndView("addUser");
    // mv.addObject("headerMessage", "Add User Details");
    // mv.addObject("user", new User());
    // return mv;
    // }
    //
    //
    // @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    // public ModelAndView saveNewUser(@ModelAttribute User user, BindingResult
    // result)
    // {
    //
    // ModelAndView mv = new ModelAndView("redirect:/home");
    //
    // if(result.hasErrors())
    // {
    // return new ModelAndView("error");
    // }
    // boolean isAdded = userService.saveUser(user);
    // if(isAdded)
    // {
    // mv.addObject("message", "New user successfully added");
    // }
    // else
    // {
    // return new ModelAndView("error");
    // }
    //
    // return mv;
    // }
    //
    //
    // @RequestMapping(value = "/editUser/{id}", method = RequestMethod.GET)
    // public ModelAndView displayEditUserForm(@PathVariable Long id)
    // {
    //
    // ModelAndView mv = new ModelAndView("/editUser");
    // User user = userService.getUserById(id);
    // mv.addObject("headerMessage", "Edit User Details");
    // mv.addObject("user", user);
    // return mv;
    // }
    //
    //
    // @RequestMapping(value = "/editUser/{id}", method = RequestMethod.POST)
    // public ModelAndView saveEditedUser(@ModelAttribute User user,
    // BindingResult result)
    // {
    //
    // ModelAndView mv = new ModelAndView("redirect:/home");
    //
    // if(result.hasErrors())
    // {
    // System.out.println(result.toString());
    // return new ModelAndView("error");
    // }
    // boolean isSaved = userService.saveUser(user);
    // if(!isSaved)
    // {
    //
    // return new ModelAndView("error");
    // }
    //
    // return mv;
    // }
    //
    //
    // @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.GET)
    // public ModelAndView deleteUserById(@PathVariable Long id)
    // {
    //
    // boolean isDeleted = userService.deleteUserById(id);
    // System.out.println("User deletion respone: " + isDeleted);
    // ModelAndView mv = new ModelAndView("redirect:/home");
    // return mv;
    //
    // }

}
