package es.cic.gestionmaquinas.controller;


import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import es.cic.gestionmaquinas.model.Maquina;
import es.cic.gestionmaquinas.model.Responsable;
import es.cic.gestionmaquinas.service.MaquinaService;
import es.cic.gestionmaquinas.service.ResponsableService;


@Controller
public class ResponsableController
{

    private static Logger log = Logger.getLogger(ResponsableController.class);

    @Autowired
    private ResponsableService responsableService;

    @Autowired
    private MaquinaService maquinaService;


    @RequestMapping(value = "/findresponsable", method = RequestMethod.GET)
    public ModelAndView searchResponsable(@RequestParam(name = "userName", required = false) String userName, Model model)
    {

	ModelAndView mv = new ModelAndView("allMaquinas");
	List<Responsable> result = null;
	log.info("Buscar responsable con el username: " + userName);
	if(userName.isEmpty())
	    result = responsableService.getAllResponsable();
	else if(!userName.isEmpty())
	    result = responsableService.getResponsablebyUserNamePartial(userName);

	if(result.size() > 0)
	{
	    model.addAttribute("responsableList", result);
	    log.info("Encontrado  responsable con el username: " + userName);
	}
	else
	{
	    model.addAttribute("error", "No se han encontrado resultados");
	    log.info("No encontrado responsable con el username: " + userName);
	}
	List<Maquina> maquinaList = maquinaService.getAllMaquinas();
	mv.addObject("maquinaList", maquinaList);

	return mv;
    }


    // editar Responsable
    @RequestMapping(value = "/editresponsable/{id}", method = RequestMethod.GET)
    public ModelAndView displayEditResponsableForm(@PathVariable Long id)
    {

	ModelAndView mv = new ModelAndView("/editResponsable");
	Responsable responsable = responsableService.getResponsableById(id);
	mv.addObject("headerMessage", "Editar responsable");
	mv.addObject("responsable", responsable);
	return mv;
    }


    @RequestMapping(value = "/editresponsable/{id}", method = RequestMethod.POST)
    public ModelAndView saveEditedResponsable(@ModelAttribute Responsable responsable, BindingResult result)
    {

	ModelAndView mv = new ModelAndView("redirect:/listallMaquinas");

	if(result.hasErrors())
	{
	    System.out.println(result.toString());
	    return new ModelAndView("error");
	}
	boolean isSaved = responsableService.saveResponsable(responsable);
	if(!isSaved)
	{

	    return new ModelAndView("error");
	}

	return mv;
    }


    @RequestMapping(value = "/deleteresponsable/{id}", method = RequestMethod.GET)
    public ModelAndView deleteResponsableById(@PathVariable Long id)
    {

	boolean isDeleted = responsableService.deleteResponsableById(id);
	System.out.println("responsable deletion response: " + isDeleted);
	ModelAndView mv = new ModelAndView("redirect:/listallMaquinas");
	return mv;

    }

    // Fin editar responsable


    // a�adir responsable

    @RequestMapping(value = "/addresponsable", method = RequestMethod.GET)
    public ModelAndView displayNewResponsalbeForm()
    {

	ModelAndView mv = new ModelAndView("/addResponsable");
	mv.addObject("headerMessage", "Nuevo Responsable");
	mv.addObject("responsable", new Responsable());
	return mv;
    }


    @RequestMapping(value = "/addresponsable", method = RequestMethod.POST)
    public ModelAndView saveNewResponsable(@ModelAttribute Responsable responsable, BindingResult result)
    {

	ModelAndView mv = new ModelAndView("redirect:/listallMaquinas");

	if(result.hasErrors())
	{
	    return new ModelAndView("error");
	}
	boolean isAdded = responsableService.saveResponsable(responsable);
	if(isAdded)
	{
	    mv.addObject("message", "Nuevo responsable a�adida exitosamente");
	}
	else
	{
	    return new ModelAndView("error");
	}

	return mv;
    }
}
