package es.cic.gestionmaquinas.controller;


import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import es.cic.gestionmaquinas.model.MaquinaConfiguration;
import es.cic.gestionmaquinas.service.MaquinaConfigurationService;
import es.cic.gestionmaquinas.service.MaquinaService;
import es.cic.gestionmaquinas.service.ResponsableService;


@Controller
public class MaquinaConfigurationController
{

    private static Logger log = Logger.getLogger(MaquinaConfigurationController.class);

    @Autowired
    private MaquinaConfigurationService maquinaConfigurationService;

    @Autowired
    private MaquinaService maquinaService;

    @Autowired
    private ResponsableService responsableService;


    @RequestMapping(value =
    { "/", "/home" }, method = RequestMethod.GET)
    public ModelAndView hello(HttpServletResponse response) throws IOException
    {

	ModelAndView mv = new ModelAndView();
	mv.setViewName("home");
	return mv;
    }


    // Get All Users
    @RequestMapping(value = "/listactive", method = RequestMethod.GET)
    public ModelAndView displayActiveList()
    {

	log.info("Mostrar las configuraciones activas");
	ModelAndView mv = new ModelAndView();
	List<MaquinaConfiguration> maquinaConfigList = maquinaConfigurationService.getActiveMaquinaConfiguration();
	mv.addObject("maquinaConfigList", maquinaConfigList);
	mv.setViewName("activeMaquinas");
	return mv;
    }


    @RequestMapping(value = "/listall", method = RequestMethod.GET)
    public ModelAndView displayAllList()
    {

	log.info("Mostrar todas las configuraciones ");
	ModelAndView mv = new ModelAndView();
	List<MaquinaConfiguration> maquinaConfigList = maquinaConfigurationService.getAllMaquinaConfiguration();
	mv.addObject("maquinaConfigList", maquinaConfigList);
	mv.setViewName("allMaquinasConfig");
	return mv;
    }


    @RequestMapping(value = "/addconfig", method = RequestMethod.GET)
    public ModelAndView displayNewMaquinaConfigurationForm()
    {

	ModelAndView mv = new ModelAndView("addMaquinaConfig");
	// mv.addObject("headerMessage", "Add User Details");
	List<String> listaMaquinas = maquinaService.getListaMaquinasFormateada();
	List<String> listaResponsables = responsableService.getResponsableUsername();

	mv.addObject("maquinaConfiguration", new MaquinaConfiguration());
	mv.addObject("listaMaquinas", listaMaquinas);
	mv.addObject("listaResponsables", listaResponsables);
	return mv;
    }


    @RequestMapping(value = "/addconfig", method = RequestMethod.POST)
    public ModelAndView saveNewMaquinaConf(@ModelAttribute MaquinaConfiguration maquinaConfiguration, BindingResult result)
    {

	ModelAndView mv = new ModelAndView("redirect:/listactive");

	if(result.hasErrors())
	{
	    return new ModelAndView("error");
	}
	boolean isAdded = maquinaConfigurationService.saveMaquinaConfiguration(maquinaConfiguration);
	if(isAdded)
	{
	    mv.addObject("message", "New user successfully added");
	}
	else
	{
	    return new ModelAndView("error");
	}

	return mv;
    }


    @RequestMapping(value = "/verhistoricodeactivo/{maquina}", method = RequestMethod.GET)
    public ModelAndView showHistoricOfActive(@PathVariable("maquina") String maquina)
    {

	log.info("Mostrar las configuraciones historicas del elemento " + maquina);
	ModelAndView mv = new ModelAndView();
	List<MaquinaConfiguration> maquinaConfigList = maquinaConfigurationService.findHistoricOfMakinaActive(maquina);
	mv.addObject("maquinaConfigList", maquinaConfigList);
	mv.setViewName("allMaquinasConfig");

	return mv;
    }


    // buscador config activas

    @RequestMapping(value = "/findconfActive", method = RequestMethod.GET)
    public ModelAndView searchMaquinaConfActive(@RequestParam(name = "maquina", required = false) String maquina, Model model)
    {

	ModelAndView mv = new ModelAndView("activeMaquinas");
	List<MaquinaConfiguration> result = null;
	log.info("Buscar M�quina con el nombre: " + maquina);
	if(maquina.isEmpty())
	    result = maquinaConfigurationService.getActiveMaquinaConfiguration();
	else if(!maquina.isEmpty())
	    result = maquinaConfigurationService.getActiveMaquinaConfigurationByNombre(maquina);

	if(result.size() > 0)
	{
	    model.addAttribute("maquinaConfigList", result);
	    log.info("Encontrado  m�quinas con el nombre: " + maquina);
	}
	else
	{
	    model.addAttribute("error", "No se han encontrado resultados");
	    log.info("No encontrado m�quinas con el nombre: " + maquina);
	}

	return mv;
    }

    // fin bucador config activas


    // buscador config todas

    @RequestMapping(value = "/findconf", method = RequestMethod.GET)
    public ModelAndView searchMaquinaConf(@RequestParam(name = "maquina", required = false) String maquina, Model model)
    {

	ModelAndView mv = new ModelAndView("allMaquinasConfig");
	List<MaquinaConfiguration> result = null;
	log.info("Buscar M�quina con el nombre: " + maquina);
	if(maquina.isEmpty())
	    result = maquinaConfigurationService.getAllMaquinaConfiguration();
	else if(!maquina.isEmpty())
	    result = maquinaConfigurationService.getAllMaquinaConfigurationByName(maquina);

	if(result.size() > 0)
	{
	    model.addAttribute("maquinaConfigList", result);
	    log.info("Encontrado  m�quinas con el nombre: " + maquina);
	}
	else
	{
	    model.addAttribute("error", "No se han encontrado resultados");
	    log.info("No encontrado m�quinas con el nombre: " + maquina);
	}

	return mv;
    }

    // fin bucador config todas

}
