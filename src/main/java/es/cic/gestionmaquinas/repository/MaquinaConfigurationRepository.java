package es.cic.gestionmaquinas.repository;


import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import es.cic.gestionmaquinas.model.MaquinaConfiguration;


@Repository
public interface MaquinaConfigurationRepository extends CrudRepository<MaquinaConfiguration, Long>
{

    @Query(value = "select * from MaquinaConfiguration m where m.maquina=?1 and m.active=true", nativeQuery = true)
    MaquinaConfiguration findMakinaActive(String name);


    @Query(value = "select * from MaquinaConfiguration m where m.active=true", nativeQuery = true)
    List<MaquinaConfiguration> findAllMakinaActive();


    @Query(value = "select * from MaquinaConfiguration m where m.maquina like ?1%", nativeQuery = true)
    List<MaquinaConfiguration> findHistoricOfMakinaActive(String maquina);


    @Query(value = "select * from MaquinaConfiguration m where m.active=true and m.maquina like ?1%", nativeQuery = true)
    List<MaquinaConfiguration> getActiveMaquinaConfigurationByNombre(String maquina);


    @Query(value = "select * from MaquinaConfiguration m where m.maquina like ?1%", nativeQuery = true)
    List<MaquinaConfiguration> getAllMaquinaConfigurationByName(String maquina);
}