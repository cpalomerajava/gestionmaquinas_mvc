package es.cic.gestionmaquinas.repository;


import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import es.cic.gestionmaquinas.model.Maquina;


@Repository
public interface MaquinaRepository extends CrudRepository<Maquina, Long>
{

    @Query(value = "select * from maquinas  where nombre like ?1%", nativeQuery = true)
    List<Maquina> getMaquinabyNamePartial(String maquina);
}