package es.cic.gestionmaquinas.repository;


import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import es.cic.gestionmaquinas.model.Responsable;


@Repository
public interface ResponsableRepository extends CrudRepository<Responsable, Long>
{

    @Query(value = "select * from responsable  where username like ?1%", nativeQuery = true)
    List<Responsable> getResponsablebyUserNamePartial(String username);


    @Query(value = "select username from responsable", nativeQuery = true)
    List<String> getResponsableUsername();
}