<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Lista de configuraciones</title>

<!-- Bootstrap -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
body {
	margin-top: 30px;
}

#buscador {
	height: 34px;
	padding-left: 5px;
}

#error{
	margin-left:10px;
	color: red;
}
.titulo{
	font-size: x-large;
    color: blue;
}

#buscadorMaquinaConf{
	display:none;
	margin-top: 20px;
}
#buscador, #buscador2 {
	height: 34px;
	padding-left: 5px;
}
 </style> 
<%-- <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css" />"> --%>

<script type="text/javascript">
  function mostrar(id)
  {
	  if (document.getElementById){ 
		  var el = document.getElementById(id); 
		  el.style.display = (el.style.display == 'block') ? 'none' : 'block'; //damos un atributo display:none que oculta el div
		  }
      
     
  }
</script>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="span12">
			<div class="titulo">Lista de configuración máquinas activas</div></br>
				<a href="home" class="btn btn-danger" role="button">Inicio</a>
				<button   class="btn btn-primary" onclick="mostrar('buscadorMaquinaConf');">Buscador</button>
				<a href="addconfig" class="btn btn-primary" role="button">Añadir</a>
				 </br>
				<table id="buscadorMaquinaConf" >
					<tr>
						<td>
							<form method="get" action="findconfActive" class="form-inline">
								<input id="buscador" name="maquina" type="text" placeholder="Nombre de máquina">
								<button type="submit" class="btn btn-primary">Buscar</button>
							</form>
						</td>
						<td><div id="error">${error}</div></td>
					</tr>
				</table>

			</div>
		</div>

		</br> </br>
		<div class="row">
			<table
				class="table table-bordered table-striped table-hover table-condensed table-responsive">
				<thead>
					<tr>
						<th>Máquina</th>
						<th>Fecha update</th>
						<th>Ultimo Script bbdd</th>
						<th>BBDD actualizada</th>
						<th>Wars actualizados</th>
						<th>Versión wars</th>
						<th>Entorno actualizado</th>
						<th>Responsable</th>

						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${maquinaConfigList}" var="listaconf">

						<tr>
							<td>${listaconf.maquina}</td>
							<td>${listaconf.fecha}</td>
							<td>${listaconf.ultimoScriptBBDD}</td>
							<td>
								<c:if test="${listaconf.updateBbdd}">SI</c:if>
								<c:if test="${!listaconf.updateBbdd}">NO</c:if>
							</td>
							<td>
								<c:if test="${listaconf.cambioWars}">SI</c:if> 
								<c:if test="${!listaconf.cambioWars}">NO</c:if>
							</td>
							<td>${listaconf.versionWar}</td>
							
							<td>
								<c:if test="${listaconf.cambioEntorno}">SI</c:if> 
								<c:if test="${!listaconf.cambioEntorno}">NO</c:if>
							</td>
							<td>${listaconf.responsable}</td>
							<td>
								<%-- 								<a href="edit/${estudiante.id}" class="btn btn-primary" role="button">Editar</a> --%>
								<a href="verhistoricodeactivo/${listaconf.maquina}" class="btn btn-danger"
								role="button">Histórico</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>


	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>