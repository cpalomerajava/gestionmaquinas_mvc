<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Añadir configuración</title>

<!-- Bootstrap -->
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<style>
body {
	margin-top: 30px;
}
select{
	height: 25px;
}

#button{
    margin-left: 129px;
    margin-top: 34px;
    width: 87px;
    height: 41px;
}

#marco{
	margin-top: -30px;
}

#intro{
    margin-left: 40%;
}
#title{
	padding-bottom: 20px;
    font-size: x-large;
    text-decoration: underline;
}

</style>
</head>
<body>

<div id="marco" class="jumbotron">
	<div id="intro" class="container">
		<div id="title">Añade una nueva configuración:</div>
		<div class="row">
			<div class=".col-md-6 .col-md-offset-3">
				<form:form method="POST" action="${not empty op ? '../edit' : 'addconfig'}"	modelAttribute="maquinaConfiguration">
					<form:hidden path="id" />
					<table>
						<tr>
							<td><form:label path="maquina">Máquina</form:label></td>
							<td>
								<select name="maquina">
								 <c:forEach var="element" items="${listaMaquinas}">
								  <option value="${element}">${element}</option>
								 </c:forEach>
								</select> 
							
							</td>
						</tr>
						<tr>
							<td><form:label path="updateBbdd">BBDD actualizado</form:label></td>
							<td><input type="checkbox" id="cbox3" name="updateBbdd"></td>
						</tr>
						<tr>
							<td><form:label path="ultimoScriptBBDD">Último script ejecutado</form:label></td>
							<td><form:input path="ultimoScriptBBDD" /></td>
						</tr>
						<tr>
							<td><form:label path="cambioWars">Wars actualizados</form:label></td>
							<td><input type="checkbox" id="cbox1" name="cambioWars"></td>
						</tr>
						<tr>
							<td><form:label path="versionWar">Versión wars</form:label></td>
							<td><form:input path="versionWar" /></td>
						</tr>
						<tr>
							<td><form:label path="cambioEntorno">Entorno actualizado</form:label></td>
							<td><input type="checkbox" id="cbox2" name="cambioEntorno"></td>
						</tr>
						<tr>
							<td><form:label path="responsable">Responsable</form:label></td>
							<td>
								<select name=responsable>
								 <c:forEach var="element" items="${listaResponsables}">
								  <option value="${element}">${element}</option>
								 </c:forEach>
								</select> 
							
							</td>
						</tr>
						<tr>
							<td><input id="button" class="btn btn-primary btn-lg" type="submit" value="Guardar" /></td>
						</tr>

					</table>
				</form:form>

			</div>
		</div>
	</div>
</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>